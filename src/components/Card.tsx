import React, { useRef } from "react";
import styles from "../app/page.module.scss";
import Image from "next/image";
import { useIntersectionObserver } from "@/Hooks/useIntersectionObserver";

interface CardProps {
  movie: {
    title: string;
    poster_image: {
      path: string;
    };
    id: string;
    duration: string;
    imdb_rank_percent: number;
    year: number;
    page_title: string;
  };
  isLast: boolean;
  onLastCardVisible: () => void;
}

const Card = ({ movie, isLast, onLastCardVisible }: CardProps) => {
  const ref = useRef<HTMLDivElement | null>(null);
  const entry = useIntersectionObserver(ref, {});
  const isVisible = !!entry?.isIntersecting;

  if (isVisible && isLast) {
    onLastCardVisible();
  }

  const durationFormatter = (time: string) => {
    if (time) {
      const parts = time?.split(":");
      const hours = parseInt(parts[0]);
      const minutes = parseInt(parts[1]);
      let duration = "";
      if (hours > 0) {
        duration += `${hours} ساعت `;
      }

      if (minutes > 0) {
        duration += `${minutes} دقیقه`;
      }

      return duration;
    }
  };

  return (
    <div key={movie.id}>
      <div className={styles.movie_container} ref={ref}>
        <Image
          alt={movie.title}
          width={206}
          height={270}
          src={movie.poster_image.path}
          className={styles.image}
          // => Tip more optimization for Image
          // placeholder="blur"
          // blurDataURL={product.featured_image}
        ></Image>
        <div className={styles.overlay}></div>

        <div className={styles.info}>
          <div>{durationFormatter(movie.duration)}</div>
          <div> IMDB {movie.imdb_rank_percent} </div>
          <div> سال ساخت {movie.year} </div>
        </div>
      </div>
      <h4>{movie.page_title}</h4>
    </div>
  );
};

export default Card;
