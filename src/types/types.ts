type CategoryItem = {
  title: string;
};

type Category = {
  type: string;
  items: CategoryItem[];
};

type VideoFiles = {
  primary: any[];
  trailers: any[];
};

type CoverImage = {
  path: string;
};

export type Movie = {
  rate: number;
  age_restriction: string;
  categories: Category[];
  published_at: string;
  status: string;
  title: string;
  page_title: string;
  slug: string;
  summary: string;
  cover_image: CoverImage;
  poster_image: CoverImage;
  logo_image: CoverImage;
  alter_cover_image: CoverImage;
  type: string;
  flag: string;
  conditional_flag: string;
  year: number;
  imdb_rank_percent: number;
  original_name: string;
  duration: string;
  video_files: VideoFiles;
  video_content_access_approach: string;
  id: string;
  short_id: string;
};
