import { api } from "../apiBase";

const queriesApi = api.injectEndpoints({
  endpoints: (builder) => ({
    getMovies: builder.query({
      query: ({ nextUrl, ...params }) => ({
        url: `https://filmnet.ir/api-v2${nextUrl}`,
        params,
      }),

      serializeQueryArgs: ({ endpointName }: { endpointName: string }) => {
        return endpointName;
      },
      merge: (currentCache: any, newItems: any) => {
        currentCache.data.push(...newItems.data);
        currentCache.meta = newItems.meta;
      },
      forceRefetch({
        currentArg,
        previousArg,
      }: {
        currentArg: string;
        previousArg: string;
      }) {
        return currentArg !== previousArg;
      },
    }),
  }),
});

export const { useGetMoviesQuery } = queriesApi;
