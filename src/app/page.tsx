"use client";
import { useState } from "react";
import styles from "./page.module.scss";
import { useGetMoviesQuery } from "../redux/rtk/queries";
import Card from "@/components/Card";
import { Movie } from "@/types/types";

// => Tip also we can fetch data from server then pass it to client component,
// => we can remove "use client" so the component change to server component then run on the server
// async function getData() {
//   const res = await fetch("https://filmnet.ir/api-v2/video-contents", {
//     next: { revalidate: 3600 },
//   });
//   if (!res.ok) {
//     throw new Error("Failed to fetch data");
//   }
//   return res.json();
// }

export default function Home() {
  const [nextUrl, setNextUrl] = useState<string>("/video-contents");
  const { data = [{}], isError } = useGetMoviesQuery({ nextUrl });

  const handleLastCardVisible = () => {
    setNextUrl(data.meta.next_url);
  };

  return (
    <div className={styles.layout}>
      {isError ? (
        <>:) Ops maybe CORS </>
      ) : (
        <div className={styles.grid_container}>
          {data?.data?.map((movie: Movie, index: number) => {
            return (
              <Card
                movie={movie}
                key={movie.id}
                isLast={index === data.data.length - 1}
                onLastCardVisible={handleLastCardVisible}
              />
            );
          })}
        </div>
      )}
    </div>
  );
}
